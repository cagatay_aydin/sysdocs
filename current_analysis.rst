################################################
Ephys data analysis stream
################################################

Currently, we are using 3 acqusition systems to acquire electrophsiological data using silicon probes.

* `Neuropixels <https://www.neuropixels.org/>`_

* `Neuralynx <https://neuralynx.com/>`_

* `Open Ephys <https://open-ephys.org/>`_

Each acqusition system generates different raw file formats. Our aim is to convert them into the same file format (raw binary; .bin/.dat) to automatically cluster by using generic tools.

.. image:: ./images/analysis_01.png


************************
Preprocessing
************************


Convert
************************

While we are converting the data to the binary format, we have to keep in mind;

1. Removing analog/digital channels
2. Reordering channels 

Cluster
************************

* spyking-circus

* kilosort

* kilosort 2

************************
Postprocessing
************************

.. _Folder structure:

Folder structure
************************
To perform batch analysis, we keep the folder structures in order as below;

.. code-block:: none

    folder structure
    ├── raw ephys data - e.g.raw_nlx_data
    ├── stimlog data - e.g.Matlab generated experiment file: Experiment_25032018_102829.mat    
    ├── tdms_data - National instruments file
    ├── raw_sniff_data - trial seq. files
    ├── sniff_data - e.g. .seq --> .mat
    ├── scGUI_data - automatic and manual clustered data from spyking circus
    ├── processed_data - postprocessed data 
    └── figures - plots and illustrations 

Each folder has identical subfolder structure that has information of date,name of the animal, experiment sessions: [date_animalname_session] (e.g. 180322_MB019_1)
    
.. code-block:: none

    subfolder structure [e.g.raw nlx data]
    ├── [date_animalname_session_1]
    ├── [date_animalname_session_2]
    ├── [date_animalname_session_3]
    ...
    └── [date_animalname_session_n]
    

Analyze
************************
We use custom written Matlab codes (a repository) to wrap preprocessed data, analyze and save as postprocessed data. Postprocessed data is formated in matlab structure which allows us to load only necessary variables to do future analysis and illustrations. In this way, we can reduce the amount of memory used for basic analysis. The postprocessed data is formatted as follows;

.. code-block:: none

    [date_animalname_session_1]
    ├── [behaviourdata] - has synced events information from tdms and raw ephys data
    ├── [odortrialdata.mat] - has odor input aligned trials
    ├── [sniffdata.mat]- has synced sniff information
    └── [spikedata.mat] - has time stamps,depth of recording, inter-spike-interval ... of each neuron

Detailed information can be found in Code repository section.
