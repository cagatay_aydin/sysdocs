################################################
Code repository
################################################

Before we start analyzing data, we have to be sure about folder organization covered in :ref:`Folder structure`.

Once the folder structure is intact, we use a batch script  ``process_data_XXX.m`` to read raw/preprocessed data;

*******************
process_data_XXX.m
*******************

.. image:: ./images/analysis_02.png



.. _User input:

User input
*******************

.. code-block:: matlab
   :linenos:

    %inside process_data_XXX.m

    % indicating the working directory
    cd e:\local\users\cagatay\aon-priform\ 

    % listing sessions to analyze in [date_animal_session] format
    experiments = {'180322_MB019',...  
    '180329_MB021'};


    % defining recording locations of each session
    region = {'aon',... 
    'piriform'};

    % defining the depth of the insertion
    bregma = [2.68,...
    2.05];

    % listing odors used in the experiment
    chemical_list = {'Blank','Anisole','Ethyl valerate',...
    'Eugenol','Limonele','2,3,5-trimethylpyrazine',...
    'Benzyl acetate','Blank','Cinnamaldehyde',...
    'Pentenoic acid','Linalyl formate','Geraniol',...
    'Thiophene','Phenethyl alcohol'};

     % indicating the analog input output channel used in neurolynx     event file for each session
    tval_idx = [1;1]; 


Folder definitions
*******************

Based on the acqusition system and the probe used in the experiment, sampling frequency and the function to extract analog/digital events might differ. To add flexibility, we can change the name of the folders if needed. 

.. code-block:: matlab
   :linenos:
    
    sort_folder = 'scGUI_data'; % output of spyking circus
    events_folder = 'raw_nlx_data'; %raw CSCn.ncs and Events.nev
    tdms_folder = 'tdms_data';
    stimlog_folder = 'stimlog_data';
    raw_nlx_folder = 'raw_nlx_data';
    processed_folder = 'processed_data';
    

Reading Analog/Digital events
*********************************

We have several configurations to read analog and digital channels;

Reading Natural Instruments .tdms
=================================

Camera and olfactomat triggers are generated from National instruments analog digital IO board.



Reading Acqusition system events .nev|.dat|.bin
=================================================

* Neuralynx
    * Events are saved as ``Events.nev``. It is a native Neuralynx file format consists of 16 bit information.
    * ``ft_read_event`` function is used to extract events information.
    * Based on the pin connection to `Digital Lynx SX <https://neuralynx.com/hardware/digital-lynx-4sx>`_  analog digital IO, ``tval_idx`` (channel) value has to be updated indicated in :ref:`User input`.

.. code-block:: matlab
   :linenos:

    % code block is taken from process_data_scGUI.m
    %LOADING NLX EVENTS
    tmp = list_files([raw_nlx_folder,'/',experiments{e}],'*.nev');
    hdr = ft_read_event(tmp{1});
    
    ss = size(hdr,1);
    tstamp = nan(ss,1);
    tval = nan(ss,1);
    for ii = 1:length(hdr)
        tstamp(ii,:) = double(hdr(ii).timestamp)/1e6; %converting to seconds
        tval(ii,:) = double(hdr(ii).value);
    end
    
    start_event = tstamp(1);
    tstamp = tstamp-start_event;
    
    idx = find(tval == tval_idx(e));
    if sum(diff(tstamp(idx))<0.1)>0
        tb = find(diff(tstamp(idx))<0.1);
        tmp_events = tstamp(idx);
        all_events = tmp_events(1:tb(1));
        fprintf('broken NLX file %d events\n',sum(diff(tstamp(idx))<0.1));
    else
        all_events = tstamp(idx);
    end



* Open ephys 
    * Events are saved as analog channels in the ``.dat`` file.
    * After removing broken channels and applying median substraction, seperate ``continuous.dat`` files are concatenated as ``concatenated.dat``.
    * ``extract_fast_oe_triggers`` function is used to extract the events information from the the channels specified as ``chidx = [68,69,70]``.
    * First,  events are extracted and if ``.mat`` is already exist in events folder, events are loaded.

.. code-block:: matlab
   :linenos:

    % code block is taken from process_data_OE.m
    %READING EVENTS FROM OPEN EPHYS DAT/BIN FILE
    events_destination = fullfile([events_folder,'/',experiments{e}]);
    tmp = list_files(events_destination,'*.mat');
    fs = 30000;
    nchannels = 75;
    chidx = [68,69,70];
    if isempty(tmp)
        % convert to mat file takes some time and memory
        raw_destination = fullfile([raw_nlx_folder,'/',experiments{e}]);
        events_destination = fullfile([events_folder,'/',experiments{e}]);
        mkdir([events_folder,'/',experiments{e}])
        ttmp = list_files(raw_destination,'concatenated.dat');
        savefile = sprintf('%s%sfast_events.mat',events_destination,filesep);
        extract_fast_oe_triggers(ttmp{1},savefile,fs,nchannels,chidx);
        %         extract_neuropixels_tiggers(raw_destination,events_destination)
        tmp = list_files(events_destination,'*.mat');
        tm = load(tmp{1});
    else
        tmp = list_files(events_destination,'*.mat');
        tm = load(tmp{1});
    end
    
    %     tm.th_70
    %for odor stimulation
    diff_events = diff(tm.th_70);
    [id,ikd] = sort(diff_events,'descend');
    all_events = tm.th_70(ikd(of_task_start(e))+1:ikd(of_task_end(e)))';

* Neuropixels 3A with FPGA
    * During acqusition, events are saved as the last channel (e.g. 385th channel) in 16 bit form.
    * ``extract_neuropixels_triggers`` function is used to extract the events information from the last channel.
    * First,  events are extracted and if ``.mat`` is already exist in events folder, events are loaded.

.. code-block:: matlab
   :linenos:

    % code block is taken from process_data_KILO.m
    % READING EVENTS FROM RAW BIN FILE
    events_destination = fullfile([events_folder,'/',experiments{e}]);
    tmp = list_files(events_destination,'*.mat');
    if isempty(tmp)
        % convert to mat file takes some time and memory
        raw_destination = fullfile([raw_nlx_folder,'/',experiments{e}]);
        events_destination = fullfile([events_folder,'/',experiments{e}]);
        mkdir([events_folder,'/',experiments{e}])
        extract_neuropixels_tiggers(raw_destination,events_destination)
        tmp = list_files(events_destination,'*.mat');
        tm = load(tmp{1});
    else
        tmp = list_files(events_destination,'*.mat');
        tm = load(tmp{1});
    end
    a = tm.allTriggersS{1}{1};
    all_events = a(diff(a)>.015);

* Neuropixels 1.0 with National instruments PXIe
    * Events are saved as the last channel (e.g. 385th channel) in as 16 bit form

.. code-block:: matlab
   :linenos:




* Neuropixels 1.0&2.0 with National instruments PXIe and NI PXIe AIO card
    * Events are saved seperate channels


*******************
get_data.m
*******************

*``behaviourdata``
    *``file_name``  
    *``exp_name``
    *``session_name``
    *``stim_type``
    *``events``
    


    