.. Haesler lab. documentation documentation master file, created by
   sphinx-quickstart on Tue Jan 14 15:03:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Haesler lab. documentation!
======================================================

.. toctree::
   :maxdepth: 2

   current_analysis
   code_repository


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
